namespace BilleniumNotes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Lecturers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Average = c.Double(nullable: false),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Department_Id = c.Int(nullable: false),
                        Lecturer_Id = c.Int(nullable: false),
                        Subject_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.Department_Id, cascadeDelete: true)
                .ForeignKey("dbo.Lecturers", t => t.Lecturer_Id, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.Subject_Id, cascadeDelete: true)
                .Index(t => t.Department_Id)
                .Index(t => t.Lecturer_Id)
                .Index(t => t.Subject_Id);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notes", "Subject_Id", "dbo.Subjects");
            DropForeignKey("dbo.Notes", "Lecturer_Id", "dbo.Lecturers");
            DropForeignKey("dbo.Notes", "Department_Id", "dbo.Departments");
            DropIndex("dbo.Notes", new[] { "Subject_Id" });
            DropIndex("dbo.Notes", new[] { "Lecturer_Id" });
            DropIndex("dbo.Notes", new[] { "Department_Id" });
            DropTable("dbo.Subjects");
            DropTable("dbo.Notes");
            DropTable("dbo.Lecturers");
            DropTable("dbo.Departments");
        }
    }
}
