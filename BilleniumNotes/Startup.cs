﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BilleniumNotes.Startup))]
namespace BilleniumNotes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
