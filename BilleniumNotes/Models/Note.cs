﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BilleniumNotes.Models
{
    public class Note
    {
        
        public int Id { get; set; }
        public string AuthorID { get; set; }
        //public List<int> Rates = new List<int>();

        public double Average { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Lecturer { get; set; }
        [Required]
        public string Subject { get; set; }
        //public int SelectedDepartmentId { get; set; }
        //public SelectList DepList { get; set; }

        //public int RatingSelected { get; set; }

        [Required]
        public string  Department { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }

        public string TextShort
        {
            get
            {
                if (Description.Length > 30)
                    return Description.Substring(0, 30) + "...";
                else
                    return Description;
            }
        }

    }

    public class NotesDBContext : DbContext
    {
        public DbSet<Note> Notes { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Rating> Ratings { get; set; }
    }
}