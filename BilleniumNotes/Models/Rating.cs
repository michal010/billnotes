﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace BilleniumNotes.Models
{
    public class Rating
    {
        public int RatingId { get; set; }
        public int RatingValue { get; set; }
        public string UserID { get; set; }
        public int NoteID { get; set; }

        public Rating()
        {

        }
    }
}