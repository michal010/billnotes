﻿using BilleniumNotes.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static BilleniumNotes.Models.Note;
using Microsoft.AspNet.Identity;

namespace BilleniumNotes.Controllers
{
    [Authorize]
    public class RatingController : Controller
    {
        private NotesDBContext db = new NotesDBContext();
        
        public double FullStarsToShow(int NoteID)
        {
            //double avr = GetAverage(NoteID);
            double avr = db.Notes.Find(NoteID).Average;
            double y = avr - Math.Floor(avr);
            return avr - y;
        }
        public int OffsetStarToShow(int NoteID)
        {
            //double avr = GetAverage(NoteID);
            double avr = db.Notes.Find(NoteID).Average;
            double x = avr - Math.Floor(avr);
            if (x == 0)
                return 0;
            if (x < 0.26)
                return 1;
            if (x < 0.51)
                return 2;
            if (x < 0.76)
                return 3;
            else
                return 0;
        }

        public double EmptyStarsToShow(int NoteID)
        {
            double x = 5 - FullStarsToShow(NoteID);
            if (OffsetStarToShow(NoteID) != 0)
                x--;
            return x;
        }
        public double GetAverage(int NoteID)
        {
            double avr = 0;
            double suma = GetRatingsValueSum(NoteID);
            double ilosc = GetRatingsCount(NoteID);
            if(ilosc != 0)
                avr = suma / ilosc;
            return avr;
        }

        public double GetRatingsCount(int NoteID)
        {
            Note note = db.Notes.Find(NoteID);
            try
            {
                List<Rating> rats = db.Ratings.Where(o => o.NoteID == NoteID).ToList();
                return rats.Count;
            }
            catch
            {
                return 0;
            }
        }

        public double GetRatingsValueSum(int NoteID)
        {
            double value = 0;
            Note note = db.Notes.Find(NoteID);
            try
            {
                List<Rating> rats = db.Ratings.Where(o => o.NoteID == NoteID).ToList();
                foreach (Rating rate in rats)
                {
                    value += rate.RatingValue;
                }
            }
            catch
            {
                value = 0;
            }
            return value;
        }
        [HttpGet]
        public bool AddRating(int rate,int NoteID)
        {
            Note note = db.Notes.Find(NoteID);
            //var userID = Microsoft.AspNet.Identity.UserManagerExtensions.FindById(User.Identity.GetUserId());
            var userID = Microsoft.AspNet.Identity.IdentityExtensions.GetUserId(User.Identity);
            //Rating rating = note.Ratings.Where(o => o.UserID == userID).FirstOrDefault();
            Rating rating = db.Ratings.Where(o => o.UserID == userID && o.NoteID == NoteID).FirstOrDefault();
            if (rating == null)
            {
                Rating r = new Rating() { UserID = userID, NoteID = note.Id, RatingValue = rate };
                db.Ratings.Add(r);
                db.SaveChanges();
            }
            else
            {
                rating.RatingValue = rate;
                db.SaveChanges();
            }
            note.Average = GetAverage(note.Id);
            db.SaveChanges();
            return true;
        }

    }
}