﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BilleniumNotes.Models;
using System.IO;
using System.Globalization;

namespace BilleniumNotes.Controllers
{
    public class NotesController : Controller
    {
        private NotesDBContext db = new NotesDBContext();

        // GET: Notes
        public ActionResult Index()
        {
            return View(db.Notes.ToList());
        }

        public ActionResult MyNotes()
        {
            var userID = Microsoft.AspNet.Identity.IdentityExtensions.GetUserId(User.Identity);
            return View(db.Notes.Where(o=>o.AuthorID == userID).ToList());
        }

        // GET: Notes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // GET: Notes/Create
   
        public ActionResult Create()
        {
            ViewBag.InsigniaList = new SelectList(db.Departments.AsEnumerable(), "Id", "Name", 1);
            return View();
        }

        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Note note, HttpPostedFileBase FileUpload)
        {
            DepartmentsController connect = new DepartmentsController();
            //note.Department = connect.GetDepartment(note.insignia);
            //ModelState.SetModelValue("Department", new ValueProviderResult(note.Department, note.Department.Id.ToString(), CultureInfo.CurrentCulture));
            if (ModelState.IsValid && FileUpload != null)
            {
                var userID = Microsoft.AspNet.Identity.IdentityExtensions.GetUserId(User.Identity);
                note.AuthorID = userID;
                db.Notes.Add(note);
                db.SaveChanges();
                UploadNote(note.Id, FileUpload);
                return RedirectToAction("Index");
            }
            ViewBag.InsigniaList = new SelectList(db.Departments.AsEnumerable(), "Id", "Name", 1);
            return View(note);
        }

        public FileResult DownloadNote(int id)
        {
            string path = Path.Combine(Server.MapPath("~/UploadUsers/NotesAttachements/" + id));
            string[] files = Directory.GetFiles(path);
            string filePath = files.FirstOrDefault();
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            var response = new FileContentResult(fileBytes, filePath);
            response.FileDownloadName = Path.GetFileName(filePath);
            return response;
        }
        private void UploadNote(int id, HttpPostedFileBase file)
        {
            var pathDir = Server.MapPath("~/UploadUsers/NotesAttachements/"+ id.ToString());
            var path = Path.Combine(Server.MapPath("~/UploadUsers/NotesAttachements/" + id.ToString()), Path.GetFileName(file.FileName));
            Directory.CreateDirectory(pathDir);
            file.SaveAs(path);
        }

        // GET: Notes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Average,Title,Description")] Note note)
        {
            if (ModelState.IsValid)
            {
                db.Entry(note).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(note);
        }

        // GET: Notes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Note note = db.Notes.Find(id);
            db.Notes.Remove(note);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
